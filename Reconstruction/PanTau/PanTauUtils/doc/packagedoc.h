/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**
@page PanTauUtils_page PanTauUtils package
 * The utils package for PanTau.
 *
 * @author Sebastian.Fleischmann@cern.ch
 *
@section PanTauUtils_@section introductionPanTauUtils Introduction
 *
 * This package contains the utility classes for PanTau.
 *
@section PanTauUtils_@section PanTauUtilsOverview Class Overview
 *   The PanTauUtils package contains the following classes:
 *
 *     - PanTau::TauClassificationUtility : Classifies TauSeeds and TruthTaus
 *
@section PanTauUtils_@section ExtrasPanTauUtils Extra Pages
 *
 **/
